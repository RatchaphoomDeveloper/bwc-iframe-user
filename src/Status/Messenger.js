import { Component } from "react";
import { LOSE_TOKEN } from "./Status";
import Swal from "sweetalert2";

class Messenger extends Component {
  static message = (data) => {
    if (data === LOSE_TOKEN)
      return Swal.fire({
        icon: "error",
        title: "กรุณาตรวจสอบรหัส Token ของคุณ",
      });
  };

  static errresmessage =(data)=>{
    return Swal.fire({
      icon: "error",
      title: data,
    });
  }

}

export default Messenger
