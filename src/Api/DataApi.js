import { Component } from "react";
import axios from "axios";
import Messenger from "../Status/Messenger";

class DataApi extends Component {
  static getTranBWCData = async (data) => {
    const result = await axios({
      url: "Gm/GetTRANBWC",
      method: "GET",
      headers: {
        Authorization: `Bearer ${"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0MTIzIiwianRpIjoiMTQxODZjYjgtZDZhMi00ZDYyLTg2NDEtYjIxNjA1Mjg1Njg3IiwiZXhwIjoxNjE3NzU0NjE1LCJpc3MiOiJEZ2FEZXYiLCJhdWQiOiJEZ2FEZXYifQ.Ms-FgjPCvFHLOH46yWXOlZ3VdDZB_9C8Z4Ibxh94ons"}`,
      },
    }).catch((err) => {
      Messenger.errresmessage(err.response.data.Message);
    });
    return result.data;
  };
}

export default DataApi