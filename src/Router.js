import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Iframechild from './components/Iframechild';
import Iframparent from './components/Iframparent';

const Routers = () => {
    return (
        <div>
            <Router>
                <Switch>
                    <Route path="/iframe/" component={Iframparent} />
                    <Route path="/iframe-child/" component={Iframechild} />
                </Switch>
            </Router>
        </div>
    )
}

export default Routers
