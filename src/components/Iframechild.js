import React, { Children } from "react";
import MaterialTable from "material-table";
import DataApi from "../Api/DataApi";

const Iframechild = () => {
    const [tranBWC,settranBWC] = React.useState([])
    const getData =async(data)=>{
        const response = await DataApi.getTranBWCData(data)
        if(response){

            settranBWC(response.TranBwcData)
        }
    }
    React.useEffect(()=>{
        window.addEventListener("message",function(e){
            if(e.origin !== "http://localhost:3336"){
               
                
                // console.log(e.data)
            }
        })
        getData()
        
        return ()=>{}
    },[])
  return (
    <div>
      <h1>สมาชิก BWC</h1>
      <MaterialTable
        columns={[
          { title: "รายชื่อสมาชิก", field: "NAME" },
          { title: "รหัสสมาชิด", field: "CODE" },
          { title: "สถานะ", field: "STATUS_NAME" },
        ]}
        data={tranBWC}
        title=""
      />
    </div>
  );
};

export default Iframechild;
