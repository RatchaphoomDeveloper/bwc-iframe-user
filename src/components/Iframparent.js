import React, { useRef, useState } from "react";

const Iframparent = () => {
  const iFrameRef = useRef(null);
  const [token, setToken] = useState("");
  const sendToken = () => {
    if (!iFrameRef.current) return;
    iFrameRef.current.contentWindow.postMessage(token, "http://localhost:3336");
  };
  return (
    <div>
      {/* <h1>Parent IFRAME</h1> */}

      {/* <textarea
        id="w3review"
        name="w3review"
        rows="4"
        cols="50"
        placeholder={"กรุณาใส่ Token ของคุณ"}
        onChange={(e) => setToken(e.target.value)}
      ></textarea>
      <button onClick={sendToken}>ยืนยัน</button> */}
      <center>
        <iframe
          ref={iFrameRef}
          src={"/iframe-child/"}
          width="50%"
          height="500"
          title="Child iFrame"
        />
      </center>
    </div>
  );
};

export default Iframparent;
